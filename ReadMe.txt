Here you have a set of scripts which can:
1) download information about WDDiscovery crashes for some period from Symologic and format it into human readable JSON file;
2) download crash dump arhives from AWS;
3) Unarchive crash dumps;
4) Transform binary crash dumps into text crash reports.

Contents:
1) "main.sh" - main script which is going to be execured by end user. Has no income parameters. Gets parameters from "create_search_job.json".
2) "create_search_job.json" - configuration file;
3) "minidump_stackwalk" - binary utility to get a text crash report from binary crash dump.
4) "cookies.txt" - temporary file created by "main.sh". Can be removed anytime.
5) "out" folder - contains results of "main.sh" executing. You can found the following files inside:
	a) "crashes.json" - human readable JSON file with a list of infos about all the crashes.
	b) "crashes" folder with a bunch of folders inside. Each folder contains info about crash: binary dump, text report and detailed text report.

Setup environment:
1) Script potentially can be run on any linux system, but ubuntu is preferred. Tested with ubuntu 16.04 LTS.
2) "sudo apt-get install curl" to install curl;
3) "sudo apt-get install jq" to install jq;
4) install "pip" if needed;
5) install aws using pip: "pip install awscli --upgrade --user";
6) configure aws profile: "aws configure --profile crash_load_profile"; Use the following data:
	AWS Access Key ID: "AKIAJISVU6VJMMNXSLXQ"
	AWS Secret Access Key: "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"
	Default region name: "us-west-2"
	Default output format: "json"
See http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html for more info.
7) Ready to use.

How to work with script:
1) Open "create_search_job.json" file and setup the settings inside:
	a) You can change query to get any data you need, not crashes only. In our case you may change "platform" field to "mac_os" to get only crashes which have happened on MacOS or to "windows" to get only crashes which have happened on Windows.
	b) Change "from" field to the start date for crashes we are going to get. Date can be into ISO 8601 format or a number of seconds from 1970.
	c) Change "to" field to the end date for crashes we are going to get. Date can be into ISO 8601 format or a number of seconds from 1970.
	d) Change "timeZone" if needed.
2) Go to command line and do the following two commands:
cd /path_to_folder_with_main_script
./main.sh
3) Done! Take a look for results.
If there are no crashes for requested period script will notify about that and exit without making any changes.
If at least one crash has been found script will create "out" folder with content as described into "Contents" section.


