#!/bin/bash

# Variables
HOST="https://api.us2.sumologic.com"
ACCESSID="suQHdd2d2nQGIh"
ACCESSKEY="7PFahzUuYfjXpVksoeTXJayAPl068l5mzwhATu0OqIkiU3HiNSFhUwPU1n1Xo3KR"
AWS_PROFILE="crash_load_profile"
MESSAGES_LIMIT=999999999

OUTPUT_DIRECTORY="out"
CRASHES_JSON_NAME="crashes.json"
CRASHES_OUTPUT_DIRECTORY="crashes"

# Constants
OPTIONS="--silent -b cookies.txt -c cookies.txt"

# Clean up
rm -rf ./$OUTPUT_DIRECTORY

# Create a search job from a JSON file.
RESULT=$(curl $OPTIONS                                                                \
          -H "Content-type: application/json"                                         \
          -H "Accept: application/json"                                               \
          -d @create_search_job.json                                                    \
          --user $ACCESSID:$ACCESSKEY                                                     \
          "$HOST/api/v1/search/jobs")
JOB_ID=$(echo $RESULT | perl -pe 's|.*"id":"(.*?)"[,}].*|\1|')
echo Search job created, id: $JOB_ID

# Wait until the search job is done.
STATE=""
until [ "$STATE" = "DONE GATHERING RESULTS" ]; do
  sleep 5
  RESULT=$(curl $OPTIONS                                                              \
            -H "Accept: application/json"                                             \
            --user $ACCESSID:$ACCESSKEY                                                   \
            "$HOST/api/v1/search/jobs/$JOB_ID")
  STATE=$(echo $RESULT | sed 's/.*"state":"\(.*\)"[,}].*/\1/')
  MESSAGES=$(echo $RESULT | perl -pe 's|.*"messageCount":(.*?)[,}].*|\1|')
  RECORDS=$(echo $RESULT | perl -pe 's|.*"recordCount":(.*?)[,}].*|\1|')
  echo Search job state: $STATE, message count: $MESSAGES, record count: $RECORDS
done

# Get crashes.
RAW_RESULT=$(curl $OPTIONS                                                                \
          -H "Accept: application/json"                                               \
          --user $ACCESSID:$ACCESSKEY                                                      \
          "$HOST/api/v1/search/jobs/$JOB_ID/messages?offset=0&limit=$MESSAGES_LIMIT")

CRASHES=$(echo $RAW_RESULT | jq -r '.messages' | jq -r '.[].map' | jq -r '._raw')

# Delete the search job.
RESULT=$(curl $OPTIONS                                                                \
          -X DELETE                                                                   \
          -H "Accept: application/json"                                               \
          --user $ACCESSID:$ACCESSKEY                                                      \
          "$HOST/api/v1/search/jobs/$JOB_ID")
JOB_ID=$(echo $RESULT | sed 's/^.*"id":"\(.*\)".*$/\1/')
echo Search job deleted, id: $JOB_ID

# Create crashes JSON file
N=`echo $CRASHES | wc -w`
echo $N crashes found.

if [ "0" -eq "$N" ]; then exit 1; fi

H3=($CRASHES)

CRASHES_FILE_NAME=./$OUTPUT_DIRECTORY/$CRASHES_JSON_NAME
mkdir -p ./$OUTPUT_DIRECTORY

echo "[" > $CRASHES_FILE_NAME
for ((i = 1; i <= $N; i++))
do
    if [ "$i" -eq "$N" ]; then OR=""; else OR=","; fi
    ITEM=${H3[$i-1]}
    echo "$ITEM$OR" >> $CRASHES_FILE_NAME
done
echo "]" >> $CRASHES_FILE_NAME

# Download crashes
CRASHNAMES=$(echo $CRASHES | jq -r '.filename')
CRASHES_DIRECTORY=./$OUTPUT_DIRECTORY/$CRASHES_OUTPUT_DIRECTORY
for NAME in $CRASHNAMES
do
    BASENAME=$(basename $NAME)
    aws --profile $AWS_PROFILE --region us-west-2 s3 cp s3://wd-portal-prod-crashdump/$NAME.gzip $CRASHES_DIRECTORY/$BASENAME/$BASENAME.gzip
done

# Symbolicate crashes
input_directory=$CRASHES_DIRECTORY

minidump_path="$(pwd)/minidump_stackwalk"
cd $input_directory

for gzip in $(find . -name '*.gzip')
do
gzip_without_extension=${gzip%.*}
unzipped_path="$gzip_without_extension.unzipped"
cat "$gzip" | gunzip > "$unzipped_path"
$minidump_path -m "$unzipped_path" > "$gzip_without_extension.txt"
$minidump_path "$unzipped_path" > "$gzip_without_extension"_detailed.txt

rm -f $unzipped_path
done

